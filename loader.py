from datasets import load_dataset
import pandas as pd


def load_hf_dataset(path):
    return load_dataset(path)


def load_basilb2s_dataset(path):
    df = pd.read_csv(path)
    return df['Text'].values, df['Language'].values
