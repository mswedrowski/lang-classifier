# Lang Classifier



## How to run

1. Install dependencies `pip install -r requirements.txt`
2. Run script `python main.py` with desired parameters

### Example of training parameters
```bash
python main.py --embedding papluca/xlm-roberta-base-language-detection
--classifier MLP 
--mode train 
--dataset_path data/language_detection.csv 
--dataset_type basilb2s
```

### Example of inference parameters
```bash
python main.py --embedding papluca/xlm-roberta-base-language-detection
--mode inference
```