from argparse import ArgumentParser

from sklearn.model_selection import train_test_split
import pickle

from classifiers.DTClassifier import DTClassifier
from classifiers.MLPClassifier import MultiLayerPerceptron
from classifiers.LogisticRegressionClassifier import LogisticRegressionClassifier
from embedders.HFEmbedder import HFEmbedder
from loader import load_basilb2s_dataset
from metrics import get_confusion_matrix, get_metrics

import yaml
from yaml.loader import SafeLoader


def get_dataset(dl_type, path):
    return {"basilb2s": load_basilb2s_dataset(path)}[dl_type]


def get_classifier(classifier, params):
    return {"DecisionTree": DTClassifier,
            "MLP": MultiLayerPerceptron,
            "lregression": LogisticRegressionClassifier}[classifier](**params)


def load_model(path):
    return pickle.load(open(path, 'rb'))


def save_model(saving_path, model):
    pickle.dump(model, open(saving_path, 'wb'))


if __name__ == '__main__':
    parser = ArgumentParser()

    parser.add_argument('--embedding', type=str, required=True, help="Huggingface model used to embed text")
    parser.add_argument('--mode', type=str, choices=['train', 'inference'], help='Specifies working mode of the script')
    # Training params
    parser.add_argument('--dataset_path', type=str, help='Path to dataset')
    parser.add_argument('--classifier', type=str, choices=['DecisionTree', 'MLP', 'lregression'],
                        help='Classifier used to '
                             'obtain prediction')
    parser.add_argument('--dataset_type', type=str, choices=['huggingface', 'basilb2s'], help="Type of dataset")
    parser.add_argument('--test_split', type=float, default=0.2, help='Size of training split')
    parser.add_argument('--model_saving_name', type=str, default='model.pkl', help='Saving name of the model')
    parser.add_argument('--metrics_saving_name', type=str, default='metrics.txt',
                        help='Saving name of the metrics file')
    parser.add_argument('--cmatrix_saving_name', type=str, default='matrix.txt', help='Saving name of file with '
                                                                                      'confusion_matrix')
    parser.add_argument('--visualization_saving_name', type=str, default=None, help='Saving name of the visualization')
    parser.add_argument('--params_file', type=str, help='Path to file containing parameters for classifier')
    parser.add_argument('--output_dir', type=str, default='output', help='Output directory')
    # Inference params
    parser.add_argument('--model_path', type=str, default='output/model.pkl', help='Loading path to the model')
    args = parser.parse_args()

    embedder = HFEmbedder(args.embedding)

    if args.mode == 'train':

        X, y = get_dataset(args.dataset_type, args.dataset_path)

        print('Embedding texts...')
        X = embedder.embed(X)

        if args.params_file:
            with open(args.params_file, 'r') as f:
                params = yaml.load(f, Loader=SafeLoader)
        else:
            params = {}
        clf = get_classifier(args.classifier, params)
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=args.test_split, random_state=42)
        print('Training...')
        clf.train(X_train, y_train)
        y_pred = clf.predict(X_test)
        get_metrics(y_test, y_pred, f'{args.output_dir}/{args.metrics_saving_name}')
        get_confusion_matrix(y_test, y_pred, f'{args.output_dir}/{args.cmatrix_saving_name}')
        save_model(f'{args.output_dir}/{args.model_saving_name}', clf)

    elif args.mode == 'inference':
        text = ''
        if not args.model_path:
            print('Inference requires \'model_path\' parameters')
        else:
            clf = load_model(args.model_path)

            while text != 'quit':
                text = input("Enter text, or 'quit': ")

                if text != 'quit':
                    embedded_text = embedder.embed([text])
                    preds = clf.predict(embedded_text)
                    print(f'Predicted: {preds}')
