from abc import ABC, abstractmethod
from typing import Iterable


class BaseEmbedder(ABC):

    @abstractmethod
    def embed(self, text: Iterable):
        pass