
from sentence_transformers import SentenceTransformer

from embedders.BaseEmbedder import BaseEmbedder


class HFEmbedder(BaseEmbedder):

    def __init__(self, hf_model):
        self.embedder = SentenceTransformer(hf_model)

    def embed(self, text):
        return self.embedder.encode(text)
