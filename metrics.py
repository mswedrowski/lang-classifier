from sklearn.metrics import classification_report, confusion_matrix


def get_metrics(y, y_pred, file):
    metrics = classification_report(y, y_pred)
    print(metrics)
    if file:
        with open(file, 'w') as f:
            print(metrics, file=f)


def get_confusion_matrix(y, y_pred, file):
    c_matrix = confusion_matrix(y, y_pred)
    print(c_matrix)
    if file:
        with open(file, 'w') as f:
            print(c_matrix, file=f)
