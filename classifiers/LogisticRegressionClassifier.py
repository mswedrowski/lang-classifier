from classifiers.BaseClassifier import BaseClassifier
from sklearn.linear_model import LogisticRegression


class LogisticRegressionClassifier(BaseClassifier):

    def __init__(self, **kwargs):
        self.model = LogisticRegression(**kwargs)

    def train(self, X, y):
        self.model.fit(X, y)

    def predict(self, X):
        return self.model.predict(X)
