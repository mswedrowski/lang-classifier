import pickle
from classifiers.BaseClassifier import BaseClassifier
from sklearn.tree import DecisionTreeClassifier


class DTClassifier(BaseClassifier):

    def __init__(self, **kwargs):
        self.model = DecisionTreeClassifier(**kwargs)

    def train(self, X, y):
        self.model.fit(X, y)

    def predict(self, X):
        return self.model.predict(X)