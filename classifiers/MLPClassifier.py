from sklearn.neural_network import MLPClassifier
from classifiers.BaseClassifier import BaseClassifier


class MultiLayerPerceptron(BaseClassifier):

    def __init__(self, **kwargs):
        self.model = MLPClassifier(**kwargs)

    def train(self, X, y):
        self.model.fit(X, y)

    def predict(self, X):
        return self.model.predict(X)